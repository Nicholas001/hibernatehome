package com.nicholas.runner;

import java.util.Scanner;

public class MeniuPrincipal {

    private MeniuProduse meniuProduse;

    public MeniuPrincipal (){
        meniuProduse = new MeniuProduse();
    }

    public void meniulPrincipal(Scanner scanner) {
        boolean test = true;

        while (test) {
            System.out.println("Meniu principal: \n");
            System.out.println("\t 1. Produse \n");
            System.out.println("\t 2. Clienti \n");
            System.out.println("\t 3. Comenzi \n");
            System.out.println("\t 9. Exit \n");
            int opt = scanner.nextInt();
            switch (opt) {
                case 1:
                    displayMeniuPiese(scanner);
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 9:
                    test = false;
                    break;
                default:
                    break;
            }
        }

    }

    public void displayMeniuPiese(Scanner scanner) {
        meniuProduse.meniuProduse(scanner);
    }

}
