package com.nicholas.runner;

import com.nicholas.util.HibernateProduse;

import java.util.Scanner;

public class MeniuProduse {

    private HibernateProduse hibernateProduse;

    public MeniuProduse() {
        hibernateProduse = new HibernateProduse();

    }

    public void meniuProduse(Scanner scanner) {

        boolean test = true;

        while (test) {
            System.out.println("Meniu produse : \n");
            System.out.println("\t 1. Inserare produs nou\n");
            System.out.println("\t 2. Afisati toate produsele \n");
            System.out.println("\t 3. Stergeti un produs dupa ID \n");
            System.out.println("\t 9. Return\n");

            int opt = scanner.nextInt();
            switch (opt) {
                case 1:
                    insertNewProdus();
                    break;
                case 2:
                    displayAllProduse();
                    break;
                case 3:
                    deleteProdus();
                    break;
                case 9:
                    test = false;
                    break;
                default:
                    break;
            }
        }
    }

    public void insertNewProdus() {
        hibernateProduse.insertProdus();
    }

    public void displayAllProduse() {
        hibernateProduse.displayAllProducts();
    }

    public void deleteProdus (){
        hibernateProduse.deleteProdus();
    }

}
