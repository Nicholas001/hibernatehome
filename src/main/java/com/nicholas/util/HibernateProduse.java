package com.nicholas.util;

import com.nicholas.entity.Produse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Scanner;


public class HibernateProduse extends HibernateUtil {

    private SessionFactory sessionFactory;
    private Session session;

    public void getSessionAndTransaction() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        session.beginTransaction();
    }

    public void commitTransactionAndCloseSession() {
       session.getTransaction().commit();
       session.close();
    }

    public void insertProdus() {
        Produse produs = new Produse();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti datele necesare pentru produsul ce urmeaza a fi inserat ");
        getSessionAndTransaction();
        System.out.println("Denumire: ");
        String denumire = scanner.nextLine();
        produs.setDenumire(denumire);
        System.out.println("Pret: ");
        int pret = scanner.nextInt();
        produs.setPret(pret);
//        session.beginTransaction();
        session.save(produs);
        commitTransactionAndCloseSession();
    }

    public void displayAllProducts() {
        getSessionAndTransaction();
//        session.beginTransaction();
        List<Produse> toateProdusle;

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Produse> criteriaQuery = criteriaBuilder.createQuery(Produse.class);
        Root<Produse> root = criteriaQuery.from(Produse.class);
        CriteriaQuery<Produse> all = criteriaQuery.select(root);

        TypedQuery<Produse> allQuery = session.createQuery(all);
        toateProdusle = allQuery.getResultList();

        for (Produse produs : toateProdusle) {
            System.out.println(produs);
        }
        commitTransactionAndCloseSession();
    }

    public void deleteProdus() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti ID-ul produsului ce urmeaza a fi sters: ");
        int id = scanner.nextInt();
        getSessionAndTransaction();
        Produse produs = session.find(Produse.class, id);
        session.delete(produs);
        commitTransactionAndCloseSession();
    }

}
