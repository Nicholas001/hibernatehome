package com.nicholas.entity;

        import javax.persistence.*;

@Entity
@Table(name = "comenzi ")
public class Comenzi {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "id_prdus")
    private int idProdus;

    @Column(name = "detalii")
    private String detalii;

    @OneToOne

    private Produse produse;

    public Comenzi() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public String getDetalii() {
        return detalii;
    }

    public void setDetalii(String detalii) {
        this.detalii = detalii;
    }

    @Override
    public String toString() {
        return "Comenzi{" +
                "id=" + id +
                ", idProdus=" + idProdus +
                ", detalii='" + detalii + '\'' +
                '}';
    }
}
