package com.nicholas.entity;

import javax.persistence.*;

@Entity
@Table(name = "Produse")
public class Produse {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "denumire")
    private String denumire;

    @Column(name = "pret")
    private int pret;

    @OneToOne

    private Comenzi comenzi;

    public Produse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    @Override
    public String toString() {
        return "Id:" + id + "; Denumire = " + denumire + ", pret=" + pret + '}';
    }


}
